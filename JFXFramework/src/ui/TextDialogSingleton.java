package ui;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;

/**
 * Created by Calvin on 9/22/2016.
 * not actually used though...
 */

public class TextDialogSingleton extends Stage {
    // HERE'S THE SINGLETON
    static TextDialogSingleton singleton;

    // GUI CONTROLS FOR OUR DIALOG
    VBox messagePane;
    Scene messageScene;
    Label messageLabel;
    Button okButton;
    Button cancelButton;
    String selection;
    TextField textRead;


    // CONSTANT CHOICES
    public static final String OK    = "Ok";
    public static final String CANCEL = "Cancel";

    /**
     * Note that the constructor is private since it follows
     * the singleton design pattern.
     */
    private TextDialogSingleton() {}

    /**
     * The static accessor method for this singleton.
     *
     * @return The singleton object for this type.
     */
    public static TextDialogSingleton getSingleton() {
        if (singleton == null)
            singleton = new TextDialogSingleton();
        return singleton;
    }

    public String getTextRead(){

        return textRead.getText();
    }


    /**
     * This method initializes the singleton for use.
     *
     * @param primaryStage The window above which this dialog will be centered.
     */
    public void init(Stage primaryStage) {
        // MAKE THIS DIALOG MODAL, MEANING OTHERS WILL WAIT
        // FOR IT WHEN IT IS DISPLAYED
        initModality(Modality.WINDOW_MODAL);
        initOwner(primaryStage);

        // LABEL TO DISPLAY THE CUSTOM MESSAGE
        messageLabel = new Label();

        // text field
        textRead = new TextField();

        // YES, NO, AND CANCEL BUTTONS
        okButton = new Button(OK);
        cancelButton = new Button(CANCEL);

        // MAKE THE EVENT HANDLER FOR THESE BUTTONS
        EventHandler<ActionEvent> CancelHandler = event -> {
            TextDialogSingleton.this.hide();
        };
        EventHandler<ActionEvent> OkHandler = event -> {
            if (!validName(textRead.getText())){
                messageLabel.setText("Please enter a valid name.");
            }
            else {
                messageLabel.setText(textRead.getText());
                TextDialogSingleton.this.hide();
            }
        };


        // AND THEN REGISTER THEM TO RESPOND TO INTERACTIONS
        okButton.setOnAction(OkHandler);
        cancelButton.setOnAction(CancelHandler);

        // NOW ORGANIZE OUR BUTTONS
        HBox buttonBox = new HBox();
        buttonBox.getChildren().add(okButton);
        buttonBox.getChildren().add(cancelButton);

        // WE'LL PUT EVERYTHING HERE
        messagePane = new VBox();
        messagePane.setAlignment(Pos.CENTER);
        messagePane.getChildren().add(messageLabel);
        messagePane.getChildren().add(textRead);
        messagePane.getChildren().add(buttonBox);

        // MAKE IT LOOK NICE
        messagePane.setPadding(new Insets(10, 20, 20, 20));
        messagePane.setSpacing(10);

        // AND PUT IT IN THE WINDOW
        messageScene = new Scene(messagePane);
        this.setScene(messageScene);
    }

    /**
     * Accessor method for getting the selection the user made.
     *
     * @return Either YES, NO, or CANCEL, depending on which
     * button the user selected when this dialog was presented.
     */
    public String getSelection() {
        return selection;
    }


    public boolean validName(String message){
        for (int i = 0; i < message.length(); i++){
            if(message.charAt(i) == ' '){
                return false;
            }
        }
        return true;
    }

    /**
     * This method loads a custom message into the label
     * then pops open the dialog.
     *
     * @param title   The title to appear in the dialog window bar.
     * @param message Message to appear inside the dialog.
     */


    public void show(String title, String message) {
        // SET THE DIALOG TITLE BAR TITLE
        setTitle(title);

        // SET THE MESSAGE TO DISPLAY TO THE USER
        messageLabel.setText(message);

        // AND OPEN UP THIS DIALOG, MAKING SURE THE APPLICATION
        // WAITS FOR IT TO BE RESOLVED BEFORE LETTING THE USER
        // DO MORE WORK.
        show();
    }

}
