package GUI;

import Data.GameData;
import Data.GameDataFile;
import apptemplate.AppTemplate;
import components.AppWorkspaceComponent;
import controller.BuzzWordController;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.*;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Line;
import propertymanager.PropertyManager;
import ui.AppGUI;
import ui.AppMessageDialogSingleton;
import ui.YesNoCancelDialogSingleton;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Array;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Random;

import static settings.InitializationParameters.APP_IMAGEDIR_PATH;
import static settings.InitializationParameters.APP_WORKDIR_PATH;


/**
 * Created by Calvin on 11/13/16.
 * end game stuff not accurate show
 * need to implement type
 * didnt test all buttons
 */
public class Workspace extends AppWorkspaceComponent {


    public AppTemplate appTemplate;
    public AppGUI GUI;

    private VBox directoryBox;
    private VBox gameInfoBox;
    private VBox endGameBox;
    private StackPane gameBox;
    public StackPane mainGirdPane;
    private GridPane loginBox;
    private GridPane createProfileBox;
    private StackPane BUZZWORD;
    private GridPane helpPane;
    private GridPane profilePane;
    private StackPane subtitlePane;
    private StackPane loadingPane;
    private StackPane pausePane;
    private StackPane currentWordPane;
    private StackPane timeRemainingPane;
    private StackPane criteriaPane;
    private StackPane targetInfoPane;
    private ScrollPane guessedWordPane;
    private StackPane levelPane;
    private StackPane totalPointPane;
    private GridPane guessedWordsGrid;
    private GridPane letterGrid;
    private GridPane hLineGrid;
    private GridPane vLineGrid;
    private GridPane dLineGrid;
    private StackPane helpRightPane;


    public Circle homeCircleGrid[][];
    public Button homeCircleButtonGrid[][];
    public Circle yellowCircleGrid[][];
    public Line hLineArray[][];
    public Line vLineArray[][];
    public Line dLineArray1[][];
    public Line dLineArray2[][];
    public Line hLineArrayYellow[][];
    public Line vLineArrayYellow[][];
    public Line dLineArray1Yellow[][];
    public Line dLineArray2Yellow[][];
    public Label homeCircleLabelGrid[][];
    public Label subTitle;
    public Label endGameMessageLabel;
    public Label timeRemainingLabel;
    public Label currentWordLabel;
    public Label levelLabel;
    public Label targetInfoLabel;
    public Label totalPointLabel;
    public Label permBuzz;
    public Label profileData;
    public Label helpData;
    public Label pauseLabel;
    public Label criteriaLabel;
    public ArrayList<Label> guessedWords;
    public ArrayList<Label> guessedWordsPoints;
    public ArrayList<String> possibleWords;
    public ArrayList<Integer> positionHistory;

    private double AppHeight = 550;
    private double AppWidth = 800;
    private double DirectoryWidth = 200;
    private double GameRightWidth = 200;
    private double GameMidWidth = 400;
    private double columnSize = 100;
    private double rowSize = 100;
    private boolean atLogin = false;
    public boolean atHelp = false;
    public boolean playing = false;
    public boolean atProfile = false;
    public boolean paused = false;
    private boolean loggedIn = false;
    private int targetPercent;
    public int targetScore;
    private int maxScore;
    private int level;
    private int timeRemaining;
    public int totalPoints;
    public long before;
    public String currentWord;


    private Button loginButton;
    private Button createProfileButton;
    private Button professaurButton;
    private Button helpButton;
    private Button startButton;
    private Button logoutButton;
    private Button homeButton;
    private Button playButton;
    private Button closeButton;
    private Button nextLevelButton;
    private Button replayLevelButton;
    private Button levelSelectionButton;
    private ComboBox selectModeButton;
    public TextField usernameField;
    public PasswordField passwordField;
    public TextField setUsernameField;
    public TextField setPasswordField;
    public String currentMode;
    private Image cursor;

    BuzzWordController controller;



    public Workspace(){

    }
    public Workspace(AppTemplate at){
        at.setWorkspaceComponent(this);
        this.appTemplate = at;
        GUI = at.getGUI();
        initializeOtherClasses();
        initializeHomeCircle();
        initButtons();
        layoutGUI();
        homeScreenInit();
        initLogin();
        initCreateProfile();
        initHelp();
        initShortcuts();
        showHomeScreen();
    }
    public void initShortcuts(){
        Workspace ws =  (Workspace)appTemplate.getWorkspaceComponent();
        final KeyCombination keyCombinationControlR = new KeyCodeCombination(KeyCode.R , KeyCombination.CONTROL_DOWN);
        final KeyCombination keyCombinationControlQ = new KeyCodeCombination(KeyCode.Q , KeyCombination.CONTROL_DOWN);
        final KeyCombination keyCombinationControlH = new KeyCodeCombination(KeyCode.H , KeyCombination.CONTROL_DOWN);


        appTemplate.getGUI().getPrimaryScene().setOnKeyPressed(new EventHandler<KeyEvent>()
        {
            @Override
            public void handle(KeyEvent ke) {
                if (ws.playing && !ws.paused) {
                    if (ke.getCode().equals(KeyCode.BACK_SPACE)) {
                        ws.processGuess('.');
                    }
                    else if (ke.getCode().equals(KeyCode.ENTER)) {
                        ws.enterGuess();
                    }
                    else if (ke.getCode().equals(KeyCode.ESCAPE)) {
                        if(ws.atProfile || ws.atHelp){
                            ws.popTop();
                        }
                    }
                    else if (keyCombinationControlR.match(ke)) {
                        ws.showGameScreen();
                    }
                    else if (keyCombinationControlQ.match(ke)) {
                        ws.closeGame();
                    }
                    else if (keyCombinationControlH.match(ke)) {
                        ws.showHomeLoggedIn();
                    }
                    else if (ke.getCode().isLetterKey()){
                        char guess = ke.getText().charAt(0);
                        if (isValid(guess)) {
                            ws.processGuess(Character.toLowerCase(guess));
                        }
                    }
                }
                else if (keyCombinationControlQ.match(ke)) {
                    ws.closeGame();
                }
                else if ( loggedIn && keyCombinationControlH.match(ke)) {

                    ws.showHomeLoggedIn();
                }
                else if (ke.getCode().equals(KeyCode.ESCAPE)) {
                    if(ws.atProfile || ws.atHelp){
                        ws.popTop();
                    }
                }

            }
        });
    }
    public boolean isValid(char c){
        if (((int)c >= 97 && (c <= 122) ||
                c >= 65 && c <= 90)){
            return true;
        }
        return false;
    }
    private void initializeOtherClasses(){
        controller = new BuzzWordController(appTemplate);
        possibleWords = new ArrayList<String>();
        positionHistory = new ArrayList<Integer>();
        timeRemaining = 60;
        before = 0;


        URL imgDirURL = AppTemplate.class.getClassLoader().getResource(APP_IMAGEDIR_PATH.getParameter());
        try (InputStream imgInputStream = Files.newInputStream(Paths.get(imgDirURL.toURI()).resolve("Cursor.png"))) {
            cursor = new Image(imgInputStream,20,20,false,false);
        } catch (URISyntaxException e) {
            e.printStackTrace();
            System.exit(1);
        } catch (IOException e) {
            e.printStackTrace();
        }



    }
    private void initializeHomeCircle(){
        homeCircleGrid = new Circle[4][4];
        yellowCircleGrid = new Circle[4][4];
        homeCircleLabelGrid = new Label[4][4];
        homeCircleButtonGrid = new Button[4][4];
        for (int i = 0; i < homeCircleGrid.length; i++){
            for (int j = 0; j < homeCircleGrid[0].length; j++){
                homeCircleGrid[i][j] = new Circle(35);
                homeCircleLabelGrid[i][j] = new Label("");
                homeCircleLabelGrid[i][j].setId("letter-text");
                yellowCircleGrid[i][j] = new Circle(45);
                yellowCircleGrid[i][j].setFill(Color.YELLOW);
                yellowCircleGrid[i][j].setVisible(false);
                homeCircleButtonGrid[i][j] = new Button();
                homeCircleButtonGrid[i][j].setId("transparent");
                homeCircleButtonGrid[i][j].setMinSize(50,50);
                homeCircleButtonGrid[i][j].setPrefSize(50,50);
                homeCircleButtonGrid[i][j].setMaxSize(50,50);
            }
        }
        hLineArray = new Line[4][3];
        hLineArrayYellow = new Line[4][3];
        for(int i = 0; i < hLineArray.length; i++){
            for (int j = 0; j < hLineArray[0].length; j++){
                hLineArray[i][j] = new Line(5,20,55,20);
                hLineArray[i][j].setId("line");
                hLineArrayYellow[i][j] = new Line(5,20,55,20);
                hLineArrayYellow[i][j].setId("yellow-line");
                hLineArrayYellow[i][j].setVisible(false);
            }
        }
        vLineArray = new Line[3][4];
        vLineArrayYellow = new Line[3][4];
        for(int i = 0; i < vLineArray.length; i++){
            for (int j = 0; j < vLineArray[0].length; j++){
                vLineArray[i][j] = new Line(20,5,20,55);
                vLineArray[i][j].setId("line");
                vLineArrayYellow[i][j] = new Line(20,5,20,55);
                vLineArrayYellow[i][j].setId("yellow-line");
                vLineArrayYellow[i][j].setVisible(false);
            }
        }
        dLineArray1 = new Line[3][3];
        dLineArray1Yellow = new Line[3][3];
        for(int i = 0; i < dLineArray1.length; i++){
            for (int j = 0; j < dLineArray1[0].length; j++){
                dLineArray1[i][j] = new Line(-10,-10,65,65);
                dLineArray1[i][j].setId("line");
                dLineArray1Yellow[i][j] = new Line(-10,-10,65,65);
                dLineArray1Yellow[i][j].setId("yellow-line");
                dLineArray1Yellow[i][j].setVisible(false);
            }
        }
        dLineArray2 = new Line[3][3];
        dLineArray2Yellow = new Line[3][3];
        for(int i = 0; i < dLineArray2.length; i++){
            for (int j = 0; j < dLineArray2[0].length; j++){
                dLineArray2[i][j] = new Line(-10,65,65,-10);
                dLineArray2[i][j].setId("line");
                dLineArray2Yellow[i][j] = new Line(-10,65,65,-10);
                dLineArray2Yellow[i][j].setId("yellow-line");
                dLineArray2Yellow[i][j].setVisible(false);
            }
        }
        level = 1;
    }
    private void initializeStack(StackPane sp, String paneStyle, Label lb, String lbText, String lbStyle, int width, int height, Pos p){

        if(paneStyle!= null) {
            sp.getStyleClass().add(paneStyle);
        }
        sp.setAlignment(p);
        lb.setText(lbText);
        if(lbStyle != null){
            lb.setId(lbStyle);
        }
        sp.getChildren().setAll(lb);
        if(width!= 0 && height!=0) {
            sp.setMinSize(width, height);
            sp.setPrefSize(width, height);
            sp.setMaxSize(width, height);
        }
    }
    private void layoutGUI(){

        workspace = new BorderPane();
        activateWorkspace(appTemplate.getGUI().getAppPane());

        directoryBox = new VBox();
        directoryBox.setPrefWidth(DirectoryWidth);
        directoryBox.getStyleClass().add("directory-pane");
        directoryBox.setPadding(new Insets(200,20,20,20));
        directoryBox.setSpacing(25);

        endGameBox = new VBox();
        endGameBox.setPrefWidth(DirectoryWidth);
        endGameBox.getStyleClass().add("gamebox-trans-pane");
        endGameBox.setPadding(new Insets(200,20,20,20));
        endGameBox.setSpacing(25);

        gameBox = new StackPane();
        gameBox.getStyleClass().add("mid-game-pane");

        gameInfoBox = new VBox();
        gameInfoBox.setPrefWidth(GameRightWidth);
        gameInfoBox.getStyleClass().add("right-game-pane");

        timeRemainingPane = new StackPane();
        currentWordPane = new StackPane();
        totalPointPane = new StackPane();
        targetInfoPane = new StackPane();
        BUZZWORD = new StackPane();
        subtitlePane = new StackPane();
        levelPane = new StackPane();
        profilePane = new GridPane();
        pausePane = new StackPane();
        criteriaPane = new StackPane();

        timeRemainingLabel = new Label();
        currentWordLabel = new Label();
        totalPointLabel = new Label();
        targetInfoLabel = new Label();
        permBuzz = new Label();
        subTitle = new Label();
        levelLabel = new Label();
        profileData = new Label();
        pauseLabel = new Label();
        criteriaLabel = new Label();
        endGameMessageLabel = new Label();

        initializeStack(timeRemainingPane,"game-help-pane",timeRemainingLabel,"Time Remaining: "+ "40" + " seconds", "game-info-text",300,30, Pos.CENTER);
        initializeStack(currentWordPane,"game-help-pane",currentWordLabel,"B U", "game-info-text",200,30, Pos.CENTER);
        initializeStack(totalPointPane,"game-help-pane",totalPointLabel,"TOTAL\t 30", "game-info-text",200,30, Pos.CENTER);
        initializeStack(targetInfoPane,"game-help-pane",targetInfoLabel,"Target:\n75 Point", "game-info-text",200,100, Pos.CENTER);
        initializeStack(BUZZWORD,"heading-pane",permBuzz,"!!BUZZWORD!!", "heading-text",0,0, Pos.CENTER);
        BUZZWORD.getChildren().add( closeButton);
        initializeStack(subtitlePane,null,subTitle,"Famous People", "subtitle-text",0,0, Pos.TOP_CENTER);
        initializeStack(levelPane,null,levelLabel,"Level: 4", "subtitle-text",0,0, Pos.BOTTOM_CENTER);
        initializeStack(pausePane,"mid-game-pane",pauseLabel,"Game is puased", "game-info-text",500,500, Pos.CENTER);
        initializeStack(criteriaPane,"game-help-pane",criteriaLabel,"Level Criteria: ", "game-info-text",300,30, Pos.CENTER);


        endGameMessageLabel.setId("subtitle-text");
/*
        loadingPane = new StackPane();
        Image image = new Image(AppTemplate.class.getClassLoader().getResource("images/Loading.png").getFile());
        ImageView iv2 = new ImageView();
        iv2.setImage(image);
        iv2.setFitWidth(400);
        iv2.setPreserveRatio(true);
        iv2.setSmooth(true);
        iv2.setCache(true);
        loadingPane.getChildren().add(iv2);
*/


        //setting up gguess words pane
        guessedWordPane = new ScrollPane();
        guessedWordsPoints = new ArrayList<Label>();
        guessedWords = new ArrayList<Label>();

        guessedWordsGrid = new GridPane();
        guessedWordsGrid.getStyleClass().add("game-help-pane");
        guessedWordsGrid.setAlignment(Pos.TOP_CENTER);
        guessedWordsGrid.setHgap(10);
        guessedWordsGrid.setVgap(2);
        guessedWordsGrid.setPrefSize(200,300);
        guessedWordsGrid.setMaxSize(200,300);
        guessedWordsGrid.setMinSize(200,300);
        guessedWordsGrid.getColumnConstraints().setAll(new ColumnConstraints(90));

        guessedWordPane.setHbarPolicy(ScrollPane.ScrollBarPolicy.NEVER);
        guessedWordPane.setVbarPolicy(ScrollPane.ScrollBarPolicy.ALWAYS);
        guessedWordPane.setPrefSize(200,300);
        guessedWordPane.setMaxSize(200,300);
        guessedWordPane.setMinSize(200,300);
        guessedWordPane.setContent(guessedWordsGrid);
        //end guessed words pane


        //initialize the line grids
        hLineGrid = new GridPane();
        hLineGrid.setPadding(new Insets(50,50,50,50));
        hLineGrid.setMaxHeight(400);
        hLineGrid.getStyleClass().add("gamebox-trans-pane");
        hLineGrid.setMaxWidth(GameMidWidth);
        hLineGrid.setMaxHeight(AppHeight);
        hLineGrid.setAlignment(Pos.CENTER);
        hLineGrid.setTranslateX(60);
        hLineGrid.setHgap(20);
        hLineGrid.setVgap(20);
        hLineGrid.getColumnConstraints().setAll(new ColumnConstraints(columnSize),new ColumnConstraints(columnSize),new ColumnConstraints(columnSize),new ColumnConstraints(columnSize));
        hLineGrid.getRowConstraints().setAll(new RowConstraints(rowSize), new RowConstraints(rowSize), new RowConstraints(rowSize), new RowConstraints(rowSize));
        for(int i = 0; i < hLineArray.length; i++){
            for (int j = 0; j < hLineArray[0].length; j++){
                StackPane sp = new StackPane();
                sp.getChildren().setAll(hLineArrayYellow[i][j], hLineArray[i][j]);
                hLineGrid.add(sp,j,i);
            }
        }
        vLineGrid = new GridPane();
        vLineGrid.setPadding(new Insets(50,50,50,50));
        vLineGrid.setMaxHeight(400);
        vLineGrid.getStyleClass().add("gamebox-trans-pane");
        vLineGrid.setMaxWidth(GameMidWidth);
        vLineGrid.setMaxHeight(AppHeight);
        vLineGrid.setAlignment(Pos.CENTER);
        vLineGrid.setTranslateY(60);
        vLineGrid.setHgap(20);
        vLineGrid.setVgap(20);
        vLineGrid.getColumnConstraints().setAll(new ColumnConstraints(columnSize),new ColumnConstraints(columnSize),new ColumnConstraints(columnSize),new ColumnConstraints(columnSize));
        vLineGrid.getRowConstraints().setAll(new RowConstraints(rowSize), new RowConstraints(rowSize), new RowConstraints(rowSize), new RowConstraints(rowSize));
        for(int i = 0; i < vLineArray.length; i++){
            for (int j = 0; j < vLineArray[0].length; j++){
                StackPane sp = new StackPane();
                sp.getChildren().setAll(vLineArrayYellow[i][j], vLineArray[i][j]);
                vLineGrid.add(sp,j,i);
            }
        }
        dLineGrid = new GridPane();
        dLineGrid.setPadding(new Insets(50,50,50,50));
        dLineGrid.setMaxHeight(400);
        dLineGrid.getStyleClass().add("gamebox-trans-pane");
        dLineGrid.setMaxWidth(GameMidWidth);
        dLineGrid.setMaxHeight(AppHeight);
        dLineGrid.setAlignment(Pos.CENTER);
        dLineGrid.setTranslateY(60);
        dLineGrid.setTranslateX(60);
        dLineGrid.setHgap(20);
        dLineGrid.setVgap(20);
        dLineGrid.getColumnConstraints().setAll(new ColumnConstraints(columnSize),new ColumnConstraints(columnSize),new ColumnConstraints(columnSize),new ColumnConstraints(columnSize));
        dLineGrid.getRowConstraints().setAll(new RowConstraints(rowSize), new RowConstraints(rowSize), new RowConstraints(rowSize), new RowConstraints(rowSize));
        for(int i = 0; i < dLineArray1.length; i++){
            for (int j = 0; j < dLineArray1[0].length; j++){
                StackPane sp = new StackPane();
                sp.getChildren().setAll(dLineArray1Yellow[i][j],dLineArray2Yellow[i][j], dLineArray1[i][j], dLineArray2[i][j]);
                dLineGrid.add(sp,j,i);
            }
        }

        mainGirdPane = new StackPane();




        workspace.setLeft(directoryBox);
        workspace.setRight(gameInfoBox);
        workspace.setCenter(gameBox);
        workspace.setTop(BUZZWORD);


    }
    public void initButtons(){
        loginButton = new Button("Login");
        loginButton.getStyleClass().add("directory-button");
        loginButton.setMinSize(150,40);

        createProfileButton = new Button("Create Profile");
        createProfileButton.getStyleClass().add("directory-button");
        createProfileButton.setMinSize(150,40);

        logoutButton = new Button("Logout");
        logoutButton.getStyleClass().add("directory-button");
        logoutButton.setMinSize(150,40);

        professaurButton = new Button("Professaur");
        professaurButton.getStyleClass().add("directory-button");
        professaurButton.setMinSize(150,40);

        helpButton = new Button("Help");
        helpButton.getStyleClass().add("directory-button");
        helpButton.setMinSize(150,40);

        startButton = new Button("Start Playing");
        startButton.getStyleClass().add("directory-button");
        startButton.setMinSize(150,40);


        closeButton = new Button("Start Playing");
        closeButton.getStyleClass().add("close-button");
        closeButton.setMinSize(40,40);
        closeButton.setMaxSize(40,40);
        closeButton.setText("X");
        closeButton.setTranslateX(650);
        closeButton.setTranslateY(-50);



        homeButton = new Button("Home");
        homeButton.getStyleClass().add("directory-button");
        homeButton.setMinSize(150,40);


        nextLevelButton = new Button();
        nextLevelButton.getStyleClass().add("directory-button");
        nextLevelButton.setMinSize(120,75);
        nextLevelButton.setMaxSize(120,75);
        nextLevelButton.setText("Next Level");

        replayLevelButton = new Button();
        replayLevelButton.getStyleClass().add("directory-button");
        replayLevelButton.setMinSize(120,75);
        replayLevelButton.setMaxSize(120,75);
        replayLevelButton.setText("Replay Level");

        levelSelectionButton = new Button();
        levelSelectionButton.getStyleClass().add("directory-button");
        levelSelectionButton.setMinSize(120,75);
        levelSelectionButton.setMaxSize(120,75);
        levelSelectionButton.setText("Select Level");

        playButton = new Button();
        playButton.getStyleClass().add("directory-button");
        playButton.setMinSize(75,75);
        playButton.setMaxSize(75,75);
        playButton.setText("Pause");
   //     playButton.setTranslateX(325);
   //     playButton.setTranslateY(0);

        selectModeButton = new ComboBox();
        GameData gd = (GameData)appTemplate.getDataComponent();
        ArrayList<String> gameMode = gd.getModeList();
        for (int i = 0; i < gameMode.size(); i++){
            selectModeButton.getItems().add(gameMode.get(i));
        }
        selectModeButton.getStyleClass().add("directory-button");
        selectModeButton.setMinSize(150,40);
        selectModeButton.setValue("\tSelect Mode");


        loginButton.setOnAction(e -> showLogin());

        homeButton.setOnAction(e-> showHomeLoggedIn());
        createProfileButton.setOnAction(e -> showCreateProfile());
        helpButton.setOnAction(e -> showHelp());
        professaurButton.setOnAction(e -> showProfile());
        startButton.setOnAction(e -> showGameScreen());
        logoutButton.setOnAction(e-> showHomeScreen());
        closeButton.setOnAction(e-> closeGame());
        playButton.setOnAction(e-> togglePlay());
        nextLevelButton.setOnAction(e-> showNextLevel());
        replayLevelButton.setOnAction(e-> showGameScreen());
        levelSelectionButton.setOnAction(e-> showLevelSelect());

    }
    public void homeScreenInit(){

        GridPane backgroundGrid = new GridPane();
        backgroundGrid.setPadding(new Insets(50,50,50,50));
        backgroundGrid.setMaxHeight(400);
        backgroundGrid.getStyleClass().add("gamebox-pane");
        backgroundGrid.setMaxWidth(GameMidWidth);
        backgroundGrid.setMaxHeight(AppHeight);
        backgroundGrid.setAlignment(Pos.CENTER);
        backgroundGrid.setHgap(20);
        backgroundGrid.setVgap(20);
        backgroundGrid.getColumnConstraints().setAll(new ColumnConstraints(columnSize),new ColumnConstraints(columnSize),new ColumnConstraints(columnSize),new ColumnConstraints(columnSize));
        backgroundGrid.getRowConstraints().setAll(new RowConstraints(rowSize), new RowConstraints(rowSize), new RowConstraints(rowSize), new RowConstraints(rowSize));



        letterGrid = new GridPane();
        letterGrid.setPadding(new Insets(50,50,50,50));
        letterGrid.setMaxHeight(400);
        letterGrid.getStyleClass().add("gamebox-trans-pane");
        letterGrid.setMaxWidth(GameMidWidth);
        letterGrid.setMaxHeight(AppHeight);
        letterGrid.setAlignment(Pos.CENTER);
        letterGrid.setHgap(20);
        letterGrid.setVgap(20);
        letterGrid.getColumnConstraints().setAll(new ColumnConstraints(columnSize),new ColumnConstraints(columnSize),new ColumnConstraints(columnSize),new ColumnConstraints(columnSize));
        letterGrid.getRowConstraints().setAll(new RowConstraints(rowSize), new RowConstraints(rowSize), new RowConstraints(rowSize), new RowConstraints(rowSize));
        for(int i = 0; i < homeCircleGrid.length; i++){
            for (int j = 0; j < homeCircleGrid[0].length; j++){
                StackPane sp = new StackPane();
                homeCircleGrid[i][j].setFill(Color.BLACK);
                homeCircleLabelGrid[i][j].setText("");
                sp.getChildren().setAll(yellowCircleGrid[i][j], homeCircleGrid[i][j], homeCircleLabelGrid[i][j],homeCircleButtonGrid[i][j]);
                letterGrid.add(sp,j,i);
            }
        }
        //reset yellow circles
        for (int i = 0; i < homeCircleGrid.length; i++){
            for (int j = 0; j < homeCircleGrid[0].length; j++){
                yellowCircleGrid[i][j].setVisible(false);
            }
        }

        homeCircleLabelGrid[0][0].setText("B");
        homeCircleLabelGrid[0][1].setText("U");
        homeCircleLabelGrid[1][0].setText("Z");
        homeCircleLabelGrid[1][1].setText("Z");
        homeCircleLabelGrid[2][2].setText("W");
        homeCircleLabelGrid[2][3].setText("O");
        homeCircleLabelGrid[3][2].setText("R");
        homeCircleLabelGrid[3][3].setText("D");


        //initialize the lines

        //mainGirdPane = new StackPane();
   //     mainGirdPane.getChildren().setAll(letterGrid, hLineGrid,vLineGrid,dLineGrid, backgroundGrid);
        mainGirdPane.getChildren().clear();
        mainGirdPane.getChildren().addAll(backgroundGrid, hLineGrid,vLineGrid,dLineGrid, letterGrid);

        selectModeButton = new ComboBox();
        GameData gd = (GameData)appTemplate.getDataComponent();
        ArrayList<String> gameMode = gd.getModeList();
        for (int i = 0; i < gameMode.size(); i++){
            selectModeButton.getItems().add(gameMode.get(i));
        }
        selectModeButton.getStyleClass().add("directory-button");
        selectModeButton.setMinSize(150,40);
        selectModeButton.setValue("\tSelect Mode");


        selectModeButton.valueProperty().addListener(new ChangeListener<String>() {
            @Override public void changed(ObservableValue ov, String t, String option) {
                showSelectMode(option);
                currentMode = option;
            }
        });

    }

    public void initLogin(){
        gameInfoBox.setSpacing(5);
        gameInfoBox.setPadding(new Insets(0,10,10,0));
        gameInfoBox.setAlignment(Pos.TOP_RIGHT);

        loginBox = new GridPane();
        loginBox.getStyleClass().add("login-pane");
        loginBox.setAlignment(Pos.CENTER);
        loginBox.setHgap(5);
        loginBox.setVgap(5);
        loginBox.getColumnConstraints().setAll(new ColumnConstraints(70),new ColumnConstraints(100));
        loginBox.getRowConstraints().setAll(new RowConstraints(25), new RowConstraints(25));
        Label name = new Label("Username:");
        Label pw = new Label("Password:");
        usernameField = new TextField();
        passwordField = new PasswordField();
        loginBox.add(name,0,0);
        loginBox.add(pw,0,1);
        loginBox.add(usernameField,1,0);
        loginBox.add(passwordField,1,1);
        loginBox.setPrefSize(300,300);
        loginBox.setMaxSize(300,300);
        loginBox.setMinSize(300,300);
        loginBox.setOnKeyPressed(new EventHandler<KeyEvent>()
        {
            @Override
            public void handle(KeyEvent ke) {
                if (atLogin && ke.getCode().equals(KeyCode.ENTER)){
                    try{
                        BuzzWordController bc = (BuzzWordController)GUI.getFileController();
                        bc.handleLogin();
                        atLogin = false;
                        showHomeLoggedIn();
                    }
                    catch (Exception e){
                        AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
                        dialog.show("Save Error", e.getMessage());
                    }

                }
                else if (atLogin && ke.getCode().equals(KeyCode.ESCAPE)){
                    gameBox.getChildren().remove(gameBox.getChildren().size()-1);
                    atLogin = false;

                }
            }
        });
    }
    public void initCreateProfile(){
        createProfileBox = new GridPane();

        createProfileBox.getStyleClass().add("login-pane");
        createProfileBox.setAlignment(Pos.CENTER);
        createProfileBox.setHgap(5);
        createProfileBox.setVgap(5);
        createProfileBox.getColumnConstraints().setAll(new ColumnConstraints(100),new ColumnConstraints(100));
        createProfileBox.getRowConstraints().setAll(new RowConstraints(25), new RowConstraints(25));
        Label name = new Label("New Username:");
        Label pw = new Label("New Password:");
        setUsernameField = new TextField();
        setPasswordField = new TextField();
        createProfileBox.add(name,0,0);
        createProfileBox.add(pw,0,1);
        createProfileBox.add(setUsernameField,1,0);
        createProfileBox.add(setPasswordField,1,1);
        createProfileBox.setPrefSize(300,300);
        createProfileBox.setMaxSize(300,300);
        createProfileBox.setMinSize(300,300);
        createProfileBox.setOnKeyPressed(new EventHandler<KeyEvent>()
        {
            @Override
            public void handle(KeyEvent ke) {
                if (atLogin && ke.getCode().equals(KeyCode.ENTER)){
                    try{
                        BuzzWordController bc = (BuzzWordController)GUI.getFileController();
                        bc.handleNewProfile();
                        atLogin = false;
                        showHomeLoggedIn();
                    }
                    catch (Exception e){
                        e.printStackTrace();
                        AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
                        dialog.show("Save Error", e.getMessage());
                       // e.printStackTrace();
                    }
                }
                else if (atLogin && ke.getCode().equals(KeyCode.ESCAPE)){
                    gameBox.getChildren().remove(gameBox.getChildren().size()-1);
                    atLogin = false;

                }
            }
        });

    }
    public void initHelp(){
        helpPane = new GridPane();
        helpPane.setPadding(new Insets(50,50,50,50));
        helpPane.getStyleClass().add("mid-game-pane");
        helpPane.setHgap(20);
        helpPane.setVgap(0);
        helpPane.getColumnConstraints().setAll(new ColumnConstraints(500));
        RowConstraints rc = new RowConstraints(100);
        helpPane.getRowConstraints().clear();
        for(int i = 0; i < 10; i++){
            helpPane.getRowConstraints().add(rc);
        }
        helpData = new Label("Background: \nWords are randomly hidden inside a grid.");
        Label label2 = new Label("\nObjective: \nConnect words to earn points.\nThe goal is to reach the target score.\nKeep in mind that there is a time limit.");
        Label label4 = new Label("\nControls: \nThere are two ways to play:\n-Use the keyboard to build the word\n-Click and drag to build a word");
        helpPane.add(helpData,0,0);
        helpPane.add(label2,0,1);
        helpPane.add(label4,0,2);


        appTemplate.getGUI().getPrimaryScene().setOnKeyPressed(new EventHandler<KeyEvent>()
        {
            @Override
            public void handle(KeyEvent ke) {
                if ((atProfile || atHelp) && ke.getCode().equals(KeyCode.ESCAPE)){
                    popTop();
                }
            }
        });


        helpRightPane = new StackPane();
        helpRightPane.getStyleClass().add("mid-game-pane");
        helpRightPane.setAlignment(Pos.CENTER);
        helpRightPane.setMinSize(300,gameInfoBox.getMinHeight());
        helpRightPane.setPrefSize(300,gameInfoBox.getPrefHeight());
        helpRightPane.setMaxSize(300,gameInfoBox.getMaxHeight());

    }
    public void popTop(){
        gameBox.getChildren().remove(gameBox.getChildren().size()-1);
        atHelp = false;
        atProfile = false;
        workspace.setRight(gameInfoBox);
    }
    public void initProfilePane(){
        GameData gd = (GameData)(appTemplate.getDataComponent());
        profilePane = new GridPane();
        profilePane.setPadding(new Insets(50,50,50,50));
        profilePane.getStyleClass().add("mid-game-pane");
        profilePane.setHgap(20);
        profilePane.setVgap(5);
        profilePane.getColumnConstraints().setAll(new ColumnConstraints(150),new ColumnConstraints(150));
        RowConstraints rc = new RowConstraints(30);
        profilePane.getRowConstraints().clear();
        for(int i = 0; i < 8+gd.getModeList().size(); i++){
            profilePane.getRowConstraints().add(rc);
        }
        profileData = new Label("Profile");
        Label profileUsernameLabel = new Label("Username");
        Label profileUsername = new Label(gd.getUsername());
        Label profilePasswordLabel = new Label("Password");
        Label profilePassword = new Label(gd.getPassword());
        Label profileModeProgressHeader = new Label("Progress:");
        profilePane.add(profileData,0,0);
        profilePane.add(profileUsernameLabel,0,1);
        profilePane.add(profileUsername,1,1);
        profilePane.add(profilePasswordLabel,0,2);
        profilePane.add(profilePassword,1,2);
        profilePane.add(profileModeProgressHeader,0,4);
        Label tempHeader;
        Label tempValue;
        for (int i = 0; i < gd.getModeList().size(); i++){
            tempHeader = new Label(gd.getModeList().get(i));
            tempValue = new Label(gd.getModeProgressList().get(i)+"");
            profilePane.add(tempHeader,0,6+i);
            profilePane.add(tempValue,1,6+i);
        }
        profilePane.add(new Label("New Password"),0, 10);
        profilePane.add(new Label("Confirm Password"),0, 11);
        PasswordField newPassword = new PasswordField();
        PasswordField confirmPassword = new PasswordField();
        profilePane.add(newPassword,1, 10);
        profilePane.add(confirmPassword,1, 11);
        Button updateButton = new Button("Update Profile");
        profilePane.add(updateButton,0,12);

        updateButton.setOnAction(e->updateProfile(newPassword.getText(), confirmPassword.getText(), profilePassword));



    }
    public void updateProfile(String newPassword, String confirmPassword, Label profilePassword){
        try {
            if (newPassword.equals(confirmPassword)) {
                GameData gd = (GameData) appTemplate.getDataComponent();
                gd.setPassword(newPassword);
                controller.handleEditRequest();
                AppMessageDialogSingleton dia = AppMessageDialogSingleton.getSingleton();
                dia.show("Save Success", "Profile Successfully Updated");
                profilePassword.setText(gd.getPassword());
            }
            else {
                throw new NullPointerException();
            }
        }catch (IOException e) {
            AppMessageDialogSingleton dia = AppMessageDialogSingleton.getSingleton();
            dia.show("Save Error", "There was a problem with the save file");
        }catch (NullPointerException e) {
            AppMessageDialogSingleton dia = AppMessageDialogSingleton.getSingleton();
            dia.show("Save Error", "Passwords do not match");
        }
    }
    public void initGuessWordsPane(){
        maxScore = 0;
        for(int i = 0; i < possibleWords.size(); i++){
            maxScore += possibleWords.get(i).length()*5;
        }
        targetPercent = 36+level*8;                    //with this rule, 8 levels max
        targetScore = maxScore*targetPercent / 100;
        targetInfoLabel.setText("Target Score: " + targetScore);
        timeRemaining = 60;

        currentWord = "";
        criteriaLabel.setText("Criteria: " + targetPercent + "% of Max");
        updateGameInfoLabels();
    }
    public void updateGameInfoLabels(){
        guessedWordsPoints.clear();
        for(int i = 0; i < guessedWords.size(); i++){
            if(guessedWords.get(i).getText().length()>2) {
                guessedWordsPoints.add(new Label((guessedWords.get(i).getText().length() * 5) + ""));
            }
            else {
                guessedWordsPoints.add(new Label("0"));
            }
        }
        guessedWordsGrid.getRowConstraints().clear();
        guessedWordsGrid.getChildren().clear();
        for (int i = 0; i < guessedWords.size(); i++){
            guessedWordsGrid.getRowConstraints().add(new RowConstraints(20));
            guessedWordsGrid.add(guessedWords.get(i),0,i);
            guessedWordsGrid.add(guessedWordsPoints.get(i),1,i);
            guessedWords.get(i).setId("game-info-text-normal");
            guessedWordsPoints.get(i).setId("game-info-text-normal");
        }
        totalPoints = 0;
        for(int i = 0; i < guessedWordsPoints.size(); i++){
            totalPoints+= Integer.parseInt(guessedWordsPoints.get(i).getText());
        }
        totalPointLabel.setText("TOTAL\t "+ totalPoints);
        timeRemainingLabel.setText("Time Remaining: " + timeRemaining + " seconds");
        currentWordLabel.setText(currentWord);
    //    timeRemainingLabel.setText("Time Remaining: " );

    }
    public void updateEndGameInfoLabels(){
        boolean contains = false;
        for (int i = 0; i < possibleWords.size(); i++){
            contains = false;
            for(int j = 0; j < guessedWords.size(); j++){
                if(guessedWords.get(j).getText().equals(possibleWords.get(i))){
                    contains = true;
                    break;
                }
            }
            if(!contains) {
                guessedWords.add(new Label(possibleWords.get(i)));
                guessedWordsPoints.add(new Label((guessedWords.get(guessedWords.size()-1).getText().length()*5)+""));
                guessedWordsGrid.getRowConstraints().add(new RowConstraints(20));
                guessedWords.get(guessedWords.size()-1).setId("game-info-text-red");
                guessedWordsPoints.get(guessedWordsPoints.size()-1).setId("game-info-text-red");
                guessedWordsGrid.add(guessedWords.get(guessedWords.size()-1), 0, guessedWordsGrid.getRowConstraints().size()-1);
                guessedWordsGrid.add(guessedWordsPoints.get(guessedWordsPoints.size()-1), 1, guessedWordsGrid.getRowConstraints().size()-1);
            }
        }
        //System.out.println(guessedWords.toString());
    }
    public void updateCurrentWordGUI(){
        resetYellowGrid();
        ArrayList<Integer> positonHistory = new ArrayList<Integer>();
        if(currentWord.length()!= 0) {
            for (int i = 0; i < 16; i++) {
                if (homeCircleLabelGrid[i / 4][i % 4].getText().charAt(0) == currentWord.charAt(0)) {
                    positonHistory.add(i);
                    highlightCurrentWord(positonHistory, currentWord, 1);
                    positonHistory.remove(positonHistory.size() - 1);
                }
            }
        }
    }
    public void resetYellowGrid(){
        for(int i =0;i<yellowCircleGrid.length;i++){
            for(int j =0;j<yellowCircleGrid[0].length;j++){
                yellowCircleGrid[i][j].setVisible(false);
            }
        }
        for(int i =0;i<hLineArrayYellow.length;i++){
            for(int j =0;j<hLineArrayYellow[0].length;j++){
                hLineArrayYellow[i][j].setVisible(false);
            }
        }
        for(int i =0;i<vLineArrayYellow.length;i++){
            for(int j =0;j<vLineArrayYellow[0].length;j++){
                vLineArrayYellow[i][j].setVisible(false);
            }
        }
        for(int i =0;i<dLineArray1Yellow.length;i++){
            for(int j =0;j<dLineArray1Yellow[0].length;j++){
                dLineArray1Yellow[i][j].setVisible(false);
            }
        }
        for(int i =0;i<dLineArray2Yellow.length;i++){
            for(int j =0;j<dLineArray2Yellow[0].length;j++){
                dLineArray2Yellow[i][j].setVisible(false);
            }
        }
    }
    public String getRelation(int core, int neighbor){
        int coreI = core/4;
        int coreJ = core%4;
        int neighborI = neighbor/4;
        int neighborJ = neighbor%4;
        if (coreI == neighborI && coreJ > neighborJ){
            return "left";
        }
        if (coreI == neighborI && coreJ < neighborJ){
            return "right";
        }
        if (coreI > neighborI && coreJ == neighborJ){
            return "up";
        }
        if (coreI < neighborI && coreJ == neighborJ){
            return "down";
        }
        if (coreI > neighborI && coreJ > neighborJ){
            return "upleft";
        }
        if (coreI < neighborI && coreJ > neighborJ){
            return "downleft";
        }
        if (coreI > neighborI && coreJ < neighborJ){
            return "upright";
        }
        if (coreI < neighborI && coreJ < neighborJ){
            return "downright";
        }
        return "nowork";
    }
    public void highlightCurrentWord(ArrayList<Integer> positionHistory, String word, int letterIndex){
        ArrayList<Integer> recPositionHistory = new ArrayList<Integer>();
        for (int i =0; i < positionHistory.size(); i++){
                recPositionHistory.add(positionHistory.get(i));
        }
        if (letterIndex >= word.length()){
            String relation = "";
            for(int i = 0; i < positionHistory.size(); i++){
                yellowCircleGrid[positionHistory.get(i)/4][positionHistory.get(i)%4].setVisible(true);
                if (i!= 0){
                    relation = getRelation(positionHistory.get(i-1),positionHistory.get(i));
                   // System.out.println(relation);
                    switch (relation){
                        case "right": hLineArrayYellow[positionHistory.get(i-1)/4][positionHistory.get(i-1)%4].setVisible(true);
                            break;
                        case "left": hLineArrayYellow[positionHistory.get(i-1)/4][positionHistory.get(i-1)%4-1].setVisible(true);
                            break;
                        case "up": vLineArrayYellow[positionHistory.get(i-1)/4-1][positionHistory.get(i-1)%4].setVisible(true);
                            break;
                        case "down": vLineArrayYellow[positionHistory.get(i-1)/4][positionHistory.get(i-1)%4].setVisible(true);
                            break;
                        case "downright": dLineArray1Yellow[positionHistory.get(i-1)/4][positionHistory.get(i-1)%4].setVisible(true);
                            break;
                        case "upleft": dLineArray1Yellow[positionHistory.get(i-1)/4-1][positionHistory.get(i-1)%4-1].setVisible(true);
                            break;
                        case "downleft": dLineArray2Yellow[positionHistory.get(i-1)/4][positionHistory.get(i-1)%4-1].setVisible(true);
                            break;
                        case "upright": dLineArray2Yellow[positionHistory.get(i-1)/4-1][positionHistory.get(i-1)%4].setVisible(true);
                            break;
                    }
                }
            }
            return;
        }
        int nextI, nextJ;
        int core = recPositionHistory.get(recPositionHistory.size()-1);
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                nextI = core/4 + i - 1;
                nextJ = core%4 + j - 1;
                if(nextI >= 4 || nextI<0 || nextJ>=4 || nextI<0){continue;}
                if (!recPositionHistory.contains(nextI*4+nextJ)&& isAdjacent(core, nextI, nextJ) && homeCircleLabelGrid[nextI][nextJ].getText().charAt(0) == word.charAt(letterIndex)) {
                    recPositionHistory.add(nextI*4+nextJ);
                    highlightCurrentWord(recPositionHistory,word,letterIndex+1);
                    recPositionHistory.remove(recPositionHistory.size()-1);
                }
            }
        }
    }
    public void highlightCurrentWord(){
        String relation;
        for(int i = 0; i < positionHistory.size(); i++){
            if( i != 0){
                relation = getRelation(positionHistory.get(i-1),positionHistory.get(i));
                // System.out.println(relation);
                switch (relation){
                    case "right": hLineArrayYellow[positionHistory.get(i-1)/4][positionHistory.get(i-1)%4].setVisible(true);
                        break;
                    case "left": hLineArrayYellow[positionHistory.get(i-1)/4][positionHistory.get(i-1)%4-1].setVisible(true);
                        break;
                    case "up": vLineArrayYellow[positionHistory.get(i-1)/4-1][positionHistory.get(i-1)%4].setVisible(true);
                        break;
                    case "down": vLineArrayYellow[positionHistory.get(i-1)/4][positionHistory.get(i-1)%4].setVisible(true);
                        break;
                    case "downright": dLineArray1Yellow[positionHistory.get(i-1)/4][positionHistory.get(i-1)%4].setVisible(true);
                        break;
                    case "upleft": dLineArray1Yellow[positionHistory.get(i-1)/4-1][positionHistory.get(i-1)%4-1].setVisible(true);
                        break;
                    case "downleft": dLineArray2Yellow[positionHistory.get(i-1)/4][positionHistory.get(i-1)%4-1].setVisible(true);
                        break;
                    case "upright": dLineArray2Yellow[positionHistory.get(i-1)/4-1][positionHistory.get(i-1)%4].setVisible(true);
                        break;
                }
            }
        }
    }
    public boolean setTimer(){
        timeRemaining--;
        updateGameInfoLabels();
        return(timeRemaining==0);
    }
    public void processGuess(char c){
        if (c=='.' && currentWord.length()>0){
            currentWord = currentWord.substring(0,currentWord.length()-1);
        }
        else if(c!='.'){
            currentWord = currentWord + c;
        }
        updateGameInfoLabels();
        updateCurrentWordGUI();
    }
    public void enterGuess(){

        AppMessageDialogSingleton dia = AppMessageDialogSingleton.getSingleton();
        //search if guess already exist and is right
        for(int i = 0; i < guessedWords.size(); i++){
            if (guessedWords.get(i).getText().equals(currentWord)){
                //THIS WORD IS ALREADY GUESSED
                dia.show("Oops","Sorry, "+currentWord + " is already guessed");
                currentWord = "";
                resetYellowGrid();
                return;
            }
        }
        GameData gd = (GameData)appTemplate.getDataComponent();
        if(possibleWords.contains(currentWord)){
            //GOOD WORD
            guessedWords.add(new Label(currentWord));
            currentWord = "";
            resetYellowGrid();
            updateGameInfoLabels();
        }
        else {
            //NOT IN DICTIONARY

            dia.show("Oops","Sorry, "+currentWord + " is not one of our words");
            currentWord = "";
            resetYellowGrid();
            updateGameInfoLabels();
        }

    }
    public void showLogin(){
        if (!atLogin && !atProfile && !atHelp){
            gameBox.getChildren().add(loginBox);
            atLogin = true;
            gameInfoBox.getChildren().clear();
            loginBox.getChildren().get(2).requestFocus();
        }
    }
    public void showHelp(){
        if (!atLogin && !atProfile && !atHelp && !playing){
            atHelp = true;
            gameBox.getChildren().add(helpPane);
            workspace.setRight(helpRightPane);
        }
        else if (!atLogin && !atProfile && !atHelp && playing && paused){
            atHelp = true;
            gameBox.getChildren().add(helpPane);
            workspace.setRight(helpRightPane);
        }
        else  if (!atLogin && !atProfile && !atHelp && playing && !paused){
            paused = true;
            showPause(paused);
            atHelp = true;
            gameBox.getChildren().add(helpPane);
            workspace.setRight(helpRightPane);
        }
    }
    public void showProfile(){
        if (!atLogin && !atHelp && !atProfile && !playing){
            initProfilePane();
            atProfile = true;

            gameBox.getChildren().add(profilePane);
            workspace.setRight(helpRightPane);
        }
        else if (!atLogin && !atHelp && !atProfile && playing && paused){
            initProfilePane();
            atProfile = true;

            gameBox.getChildren().add(profilePane);
            workspace.setRight(helpRightPane);
        }
        else if (!atLogin && !atHelp && !atProfile && playing && !paused){
            paused = true;
            showPause(paused);
            initProfilePane();
            atProfile = true;
            gameBox.getChildren().add(profilePane);
            workspace.setRight(helpRightPane);
        }
    }

    public void showCreateProfile(){
        if (!atLogin && !atProfile && !atHelp){
            gameBox.getChildren().add(createProfileBox);
            atLogin = true;
            gameInfoBox.getChildren().clear();

            createProfileBox.getChildren().get(2).requestFocus();
        }
    }
    public void showHomeScreen() {

        if (!atLogin && !atProfile && !atHelp) {
            loggedIn = false;
            playing = false;
            homeScreenInit();
            gameBox.getChildren().setAll(mainGirdPane);
            directoryBox.getChildren().setAll(loginButton, createProfileButton, helpButton);
            gameInfoBox.getChildren().clear();
        }
    }

    public void showHomeLoggedIn() {
        loggedIn = true;
        GameData gd = (GameData)appTemplate.getDataComponent();
        professaurButton.setText(gd.getUsername());
        if(playing && !atProfile && !atHelp){
            YesNoCancelDialogSingleton dia = new YesNoCancelDialogSingleton().getSingleton();
            dia.show("Warning", "You are still in a game, are you sure you want to exit?");
            if (dia.getSelection().equals("Yes")){
                playing = false;
                homeScreenInit();
                directoryBox.getChildren().setAll(professaurButton, selectModeButton, startButton, helpButton, logoutButton);
                gameBox.getChildren().setAll(mainGirdPane);
                gameInfoBox.getChildren().clear();
            }
        }
        else if (!atLogin && !atProfile && !atHelp) {
            playing = false;
            homeScreenInit();
            directoryBox.getChildren().setAll(professaurButton, selectModeButton, startButton, helpButton, logoutButton);
            gameBox.getChildren().setAll(mainGirdPane);
            gameInfoBox.getChildren().clear();
        }
    }

    public void showSelectMode(String option){
        homeScreenInit();
        if (!atLogin && !atProfile && !atHelp) {
            currentMode = option;
            resetGameGridButtons();
            playing = false;
            subTitle.setText(option);
            letterGrid.getChildren().clear();
            gameInfoBox.getChildren().clear();
            GameData gd = (GameData)appTemplate.getDataComponent();
            int modeIndex = -1;
            for(int i = 0; i < gd.getModeList().size(); i ++){
                if (gd.getModeList().get(i).equals(option)){
                    modeIndex = i;
                    break;
                }
            }
            if(modeIndex == -1) { System.out.println("this should be impossible to reach, modeIndex is not found"); }
            int currentCursor = 0;
            int maxReachedCursor = gd.getModeProgressList().get(modeIndex);
            int maxCursor = 8;
            for (int i = 0; i < homeCircleGrid.length; i++) {
                for (int j = 0; j < homeCircleGrid[0].length; j++) {
                    if(currentCursor < maxCursor) {
                        StackPane sp = new StackPane();
                        Label tempLabel = new Label(currentCursor + 1 + "");
                        tempLabel.setId("letter-text");
                        sp.getChildren().setAll(homeCircleGrid[i][j], tempLabel,homeCircleButtonGrid[i][j]);
                        if (currentCursor<maxReachedCursor) {
                            homeCircleGrid[i][j].setFill(Color.BLUEVIOLET);
                            homeCircleButtonGrid[i][j].setOnAction(e-> startGame(sp));
                        }
                        else {
                            homeCircleGrid[i][j].setFill(Color.BLACK);
                        }
                        letterGrid.add(sp, j, i);
                        currentCursor++;
                    }
                }
            }

            GridPane backgroundGrid = new GridPane();
            backgroundGrid.setPadding(new Insets(50,50,50,50));
            backgroundGrid.setMaxHeight(400);
            backgroundGrid.getStyleClass().add("gamebox-pane");
            backgroundGrid.setMaxWidth(GameMidWidth);
            backgroundGrid.setMaxHeight(AppHeight);
            backgroundGrid.setAlignment(Pos.CENTER);
            backgroundGrid.setHgap(20);
            backgroundGrid.setVgap(20);
            backgroundGrid.getColumnConstraints().setAll(new ColumnConstraints(columnSize),new ColumnConstraints(columnSize),new ColumnConstraints(columnSize),new ColumnConstraints(columnSize));
            backgroundGrid.getRowConstraints().setAll(new RowConstraints(rowSize), new RowConstraints(rowSize), new RowConstraints(rowSize), new RowConstraints(rowSize));


            mainGirdPane.getChildren().clear();
            mainGirdPane.getChildren().setAll(backgroundGrid, letterGrid);

            gameBox.getChildren().setAll(subtitlePane, mainGirdPane);
            try {
                ((GameDataFile) appTemplate.getFileComponent()).loadWordPool(appTemplate.getDataComponent(), currentMode);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
    public void startGame(StackPane sp){
        Label temp = (Label)sp.getChildren().get(sp.getChildren().size()-2);
        level = Integer.parseInt(temp.getText());
        showGameScreen();
    }
    public void noAction(){

    }
    public void showLoading(boolean loading){
        if(loading){
            gameBox.getChildren().add(loadingPane);
            workspace.setRight(helpRightPane);
        }
        else {
            gameBox.getChildren().remove(gameBox.getChildren().size()-1);
            workspace.setRight(gameInfoBox);
        }
    }
    public void showPause(boolean pause){
        if(pause){
            gameBox.getChildren().add(pausePane);
            playButton.setText("Play");
        }
        else {
            gameBox.getChildren().remove(gameBox.getChildren().size()-1);
            playButton.setText("Pause");
        }
    }
    public void showGameScreen() {
        if(mainGirdPane.getChildren().contains(endGameBox)){
            int index = 0;
            for (;index < mainGirdPane.getChildren().size(); index++){
                if (mainGirdPane.getChildren().get(index).equals(endGameBox)){
                    mainGirdPane.getChildren().remove(index);
                    break;
                }
            }
        }
        if (currentMode == null){
            AppMessageDialogSingleton dia = AppMessageDialogSingleton.getSingleton();
            dia.show("Oops", "Please select a game mode first");
            return;
        }
        if (!atLogin && !atProfile && !atHelp) {
            try {
                resetYellowGrid();
                directoryBox.getChildren().setAll(homeButton, professaurButton, helpButton);
                gameInfoBox.getChildren().setAll(timeRemainingPane, criteriaPane, currentWordPane, guessedWordPane, totalPointPane, targetInfoPane, playButton, replayLevelButton);
                // load level stuff
                homeScreenInit();
            //    showLoading(true);
                boolean goodBoard = buildGameGrid();

                while(!goodBoard){
                    buildGameGrid();
                    System.out.println("Next Iteration");
                }
                fillEmptyGameGrid();
                searchGameGrid();
            //    showLoading(false);
                guessedWords.clear();
                initGuessWordsPane();
                levelLabel.setText("Level "+ level);
                subTitle.setText(currentMode);
                gameBox.getChildren().setAll(subtitlePane, levelPane, mainGirdPane);
                playButton.setText("Pause");
                playing = true;
                paused = false;
                BuzzWordController bc = (BuzzWordController)GUI.getFileController();
                bc.handlePlay();
            }catch (Exception e){
                AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
                dialog.show("Load Error", e.getMessage());
          //      e.printStackTrace();
            }

        }
    }
    public void showNextLevel(){
        level +=1;

        showGameScreen();
    }
    public void setValue(ArrayList<Integer> list, int index, int value){
        ArrayList<Integer> temp = new ArrayList<Integer>();
     //   System.out.println("list: " + list.toString() + "\t\ttemp: " + temp.toString());
        for (int i = list.size()-1; i > index; i--){
            temp.add(list.get(list.size()-1));
            list.remove(list.size()-1);

   //         System.out.println("list: " + list.toString() + "\t\ttemp: " + temp.toString());
        }
        list.remove(list.size()-1);
        list.add(value);
    //    System.out.println("list: " + list.toString() + "\t\ttemp: " + temp.toString());
        for (int i = temp.size(); i > 0; i--){
            list.add(temp.get(temp.size()-1));
            temp.remove(temp.size()-1);
      //      System.out.println("list: " + list.toString() + "\t\ttemp: " + temp.toString());
        }
     //   System.out.println("list: " + list.toString() + "\t\ttemp: " + temp.toString());
    }
    public void handleEndGame(boolean win){
        // next level, replay level, select level
        System.out.println("game is ending " + win);
        resetGameGridButtons();
        mainGirdPane.getChildren().add(endGameBox);
        endGameBox.getChildren().clear();
        endGameBox.setAlignment(Pos.TOP_RIGHT);
        subTitle.setText("You Lose!");
        if(win){
            subTitle.setText("You Win!");

            GameData gd = (GameData)appTemplate.getDataComponent();
            int index = 0;
            while (!gd.getModeList().get(index).equals(currentMode)){
                index++;
            }
            int[][] tempHighScore = gd.getHighScoreList();
            if(tempHighScore[index][level-1] < totalPoints){
                AppMessageDialogSingleton dia = AppMessageDialogSingleton.getSingleton();
                dia.show("Personal Best", "WOW!\nYou set a new personal best of " + totalPoints + " points!");
                tempHighScore[index][level-1] = totalPoints;
            }

            if(level < 8){
                //update player info
                BuzzWordController bc = (BuzzWordController)appTemplate.getGUI().getFileController();
                for(int i = 0; i < gd.getModeList().size(); i++){
                    if (gd.getModeList().get(i).equals(currentMode) && gd.getModeProgressList().get(i) == level){
                        setValue(gd.getModeProgressList(),i,level+1);
                    }
                }
                try {
                    bc.handleEditRequest();
                } catch (Exception e){
                    AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
                    dialog.show("Save Error", e.getMessage());
                     e.printStackTrace();
                }
                endGameBox.getChildren().add(nextLevelButton);
            }
        }
        endGameBox.getChildren().add(levelSelectionButton);

    }
    public void fillEmptyGameGrid(){
        for (int i = 0; i < homeCircleLabelGrid.length; i++){
            for (int j = 0; j < homeCircleGrid[0].length; j++){
                if(homeCircleLabelGrid[i][j].getText().equals("0")){
                    homeCircleLabelGrid[i][j].setText((char)(new Random().nextInt(26)+97)+"");
                }
            }
        }
    }
    public void closeGame(){
        if (!atLogin && !playing &&!atHelp && !atProfile){
            System.exit(0);
        }
        if (!atLogin && playing &&!atHelp &&!atProfile){
            if(!paused){
                showPause(true);
                paused = true;
                playButton.setText("Play");
            }
            YesNoCancelDialogSingleton dia = new YesNoCancelDialogSingleton().getSingleton();
            dia.show("Warning", "You are still in a game, are you sure you want to exit?");
            if (dia.getSelection().equals("Yes")){
                System.exit(0);
            }
        }
    }
    public void togglePlay(){
        if(playButton.getText().equals("Pause")){
            paused = true;
            showPause(paused);
        }
        else {
            paused = false;
            showPause(paused);
        }
    }
    public boolean buildGameGrid(){
        //pick random words to render
        GameData gd = (GameData)appTemplate.getDataComponent();

        int randNum = new Random().nextInt(gd.getWordPool().size());
        int possibleWordsNum = 0;
        int maxPossibleWords = 3;
        possibleWords.clear();
        while(possibleWordsNum < maxPossibleWords){
            randNum = new Random().nextInt(gd.getWordPool().size());
            if(!possibleWords.contains(gd.getWordPool().get(randNum))){
                possibleWords.add(gd.getWordPool().get(randNum));
                possibleWordsNum++;
            }
          //  System.out.println("in loop");
        }
        if (possibleWords.size() == 0) { return false;}
        ArrayList<Character> uniqueLetters = new ArrayList<Character>();
        for (int i = 0; i < possibleWords.size(); i++){
            for(int j = 0; j < possibleWords.get(i).length(); j++){
                if (!uniqueLetters.contains(possibleWords.get(i).charAt(j))) {
                    uniqueLetters.add(possibleWords.get(i).charAt(j));
                }
            }
        }
        System.out.println(possibleWords.toString() + "   " + uniqueLetters.size());
        if (uniqueLetters.size() >= 14){ return false;}

        //now to build actual grid
        char[][] gridArray = new char[homeCircleGrid.length][homeCircleGrid.length];
        for(int i = 0; i < gridArray[0].length; i++){
            for(int j = 0; j < gridArray.length; j++){
                gridArray[j][i] = '0';
                //here, i am using the number 0 for having no character put in yet
            }
        }
        return buildGameGridRec(gridArray,possibleWords.get(0),0,0,null,"0");
    }

    public void showLevelSelect(){
        showHomeLoggedIn();
        showSelectMode(currentMode);
    }
    public void searchGameGrid(){
        GameData gd = (GameData)appTemplate.getDataComponent();
        for (int i = 0; i < gd.getWordPool().size(); i++){
            if (!possibleWords.contains(gd.getWordPool().get(i))&& gridHasWord(gd.getWordPool().get(i))){
                possibleWords.add(gd.getWordPool().get(i));
            }
        }
        System.out.println(possibleWords.toString());
    }
    public boolean gridHasWord(String word){
        ArrayList<Integer> positionHistory = new ArrayList<Integer>();
        for (int position = 0; position<16; position++){
            if(homeCircleLabelGrid[position/4][position%4].getText().charAt(0)==word.charAt(0)){
                if( gridHasWordRec(position,word,1,positionHistory)){
                    return true;
                }
            }
        }
        return false;
    }
    public boolean gridHasWordRec(int core, String word, int letterIndex, ArrayList<Integer> positionHistory){
        if(word.length() ==  letterIndex){
            return true;
        }
        ArrayList<Integer> recPositionHistory = new ArrayList<Integer>();
        if(positionHistory == null) {
            recPositionHistory = new ArrayList<Integer>();
        }
        else {
            for (int i =0; i < positionHistory.size(); i++){
                recPositionHistory.add(positionHistory.get(i));
            }
        }
        //find next
        int nextI, nextJ;
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                nextI = core/4 + i - 1;
                nextJ = core%4 + j - 1;
                if(nextI >= 4 || nextI<0 || nextJ>=4 || nextI<0){continue;}
                if (!recPositionHistory.contains(nextI*4+nextJ)&& isAdjacent(core, nextI, nextJ) && homeCircleLabelGrid[nextI][nextJ].getText().charAt(0) == word.charAt(letterIndex)) {
                    recPositionHistory.add(nextI*4+nextJ);
                    if(gridHasWordRec((nextI*4+nextJ),word,letterIndex+1,recPositionHistory)){
                        return true;
                    }
                    recPositionHistory.remove(recPositionHistory.size()-1);
                }
            }
        }
        return false;

    }
    public void buildGameGridButtons(char[][] gridArray){
        EventHandler<MouseEvent > handlerDragDetected = new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                currentWord = "";
                updateCurrentWordGUI();
                updateGameInfoLabels();
                Dragboard db = appTemplate.getGUI().getPrimaryScene().startDragAndDrop(TransferMode.ANY);
                ClipboardContent content = new ClipboardContent();
                content.putString("hello");
                db.setContent(content);
                db.setDragView(cursor);
                event.consume();
            }
        };
        EventHandler<MouseEvent > handlerDragDetectedButton = new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                currentWord = "";
                updateCurrentWordGUI();
                updateGameInfoLabels();
                Dragboard db = appTemplate.getGUI().getPrimaryScene().startDragAndDrop(TransferMode.ANY);
                ClipboardContent content = new ClipboardContent();
                content.putString("hello");
                db.setContent(content);
                db.setDragView(cursor);
                Button source = (Button) event.getSource();
                StackPane sp = (StackPane) source.getParent();
                int position = Integer.parseInt(((Button) event.getSource()).getText());
                char letter = ((Label) (sp.getChildren().get(sp.getChildren().size() - 2))).getText().charAt(0);
                yellowCircleGrid[position / 4][position % 4].setVisible(true);
                positionHistory.add(position);
                currentWord = currentWord+letter;
                updateGameInfoLabels();
                //handle add letter
                event.consume();

            }
        };
        EventHandler<DragEvent> handlerDragDrop = new EventHandler<DragEvent>(){
            @Override
            public void handle(DragEvent event) {
                positionHistory.clear();
                if(currentWord.length() != 0){
                    enterGuess();
                }
                resetHighlight();
                resetYellowGrid();

                event.setDropCompleted(true);
                //handle the word guess
                event.consume();
            }
        };
        EventHandler<DragEvent> handlerDragOver = new EventHandler<DragEvent>(){
            @Override
            public void handle(DragEvent event) {
                event.acceptTransferModes(TransferMode.COPY_OR_MOVE);
                event.consume();
            }
        };

        EventHandler<DragEvent> handlerDragEnter = new EventHandler<DragEvent>() {
            @Override
            public void handle(DragEvent event) {
                Button source = (Button) event.getSource();
                StackPane sp = (StackPane) source.getParent();
                int position = Integer.parseInt(((Button) event.getSource()).getText());
                char letter = ((Label) (sp.getChildren().get(sp.getChildren().size() - 2))).getText().charAt(0);
                if(positionHistory.size() == 0) {
                    yellowCircleGrid[position / 4][position % 4].setVisible(true);
                    positionHistory.add(position);
                    currentWord = currentWord+letter;
                    updateGameInfoLabels();
                    highlightCurrentWord();
                    //add letter
                }
                else if (!positionHistory.contains(position) && isAdjacent(positionHistory.get(positionHistory.size()-1),position/4,position%4)){
                    yellowCircleGrid[position/4][position%4].setVisible(true);
                    positionHistory.add(position);
                    currentWord = currentWord+letter;
                    updateGameInfoLabels();
                    highlightCurrentWord();
                    //add letter
                }
                event.consume();
            }
        };
        resetGameGridButtons();

        appTemplate.getGUI().getPrimaryScene().setOnDragDetected(handlerDragDetected);
        appTemplate.getGUI().getPrimaryScene().setOnDragDropped(handlerDragDrop);
        appTemplate.getGUI().getPrimaryScene().setOnDragOver(handlerDragOver);

        for(int i = 0; i < homeCircleGrid.length; i++){
            for(int j = 0; j < homeCircleGrid[0].length; j++){
                homeCircleLabelGrid[i][j].setText(gridArray[i][j]+"");
                Button temp = homeCircleButtonGrid[i][j];
                temp.setText(i*4+j+"");
                temp.setOnDragDone(handlerDragDrop);

                temp.setOnDragDetected(handlerDragDetectedButton);

                temp.setOnDragEntered(handlerDragEnter);
                temp.setOnDragOver(handlerDragOver);
            }
        }
    }
    public void resetHighlight(){
        for(int i = 0; i < homeCircleGrid.length; i++){
            for(int j = 0; j < homeCircleGrid[0].length; j++){
                yellowCircleGrid[i][j].setVisible(false);
            }
        }

    }
    public void resetGameGridButtons(){
        EventHandler<MouseEvent> handlerDragNothing = new EventHandler<MouseEvent>(){
            @Override
            public void handle(MouseEvent event) {
            }
        };
        EventHandler<DragEvent> handlerClickNothing = new EventHandler<DragEvent>(){
            @Override
            public void handle(DragEvent event) {
            }
        };
        appTemplate.getGUI().getPrimaryScene().setOnDragDetected(handlerDragNothing);
        appTemplate.getGUI().getPrimaryScene().setOnDragDropped(handlerClickNothing);
        appTemplate.getGUI().getPrimaryScene().setOnDragOver(handlerClickNothing);
        for(int i = 0; i < homeCircleGrid.length; i++){
            for(int j = 0; j < homeCircleGrid[0].length; j++){
                homeCircleButtonGrid[i][j].setOnAction(e->noAction());
                homeCircleButtonGrid[i][j].setOnDragDone(handlerClickNothing);
                homeCircleButtonGrid[i][j].setOnDragDetected(handlerDragNothing);
                homeCircleButtonGrid[i][j].setOnDragEntered(handlerClickNothing);
                homeCircleButtonGrid[i][j].setOnDragOver(handlerClickNothing);
            }
        }
    }
    public boolean buildGameGridRec (char[][] gridArray, String word, int wordIndex, int letterIndex, ArrayList<Integer> positionHistory, String iteration){
        //copy over grid so this rec method has a copy of the building array
  //      if(wordIndex==0){System.out.println(iteration);}
        char[][] recGridArray = new char[gridArray.length][gridArray[0].length];
        ArrayList<Integer> recPositionHistory = new ArrayList<Integer>();
        if(positionHistory == null) {
            recPositionHistory = new ArrayList<Integer>();
        }
        else {
            for (int i =0; i < positionHistory.size(); i++){
                recPositionHistory.add(positionHistory.get(i));
            }
        }
        for(int i = 0; i < gridArray[0].length; i++){
            for(int j = 0; j < gridArray.length; j++){
                recGridArray[j][i] = gridArray[j][i];
            }
        }
        //stop condition ||   last word is fully built
        if (wordIndex == possibleWords.size()-1 && letterIndex == possibleWords.get(possibleWords.size()-1).length()){
            // copy down this good array
            buildGameGridButtons(recGridArray);
            return true;
        }
        //putting in the next letter
        if(letterIndex < word.length()){
            int size = recGridArray.length;
            int randNum = new Random().nextInt(2);
            if (randNum == 0 || randNum == 1 ){ //look for already exist letter first
                int position = new Random().nextInt(size*size);   // start at random point in grid to look for letter
                for(int i = 0; i < size*size; i++){
                    if(((recPositionHistory.size() == 0) ||             //can look anywhere since not last position
                            (recPositionHistory.size()!=0 && isAdjacent(recPositionHistory.get(recPositionHistory.size()-1),position/size, position%size)))&&    //has last position, must be adjacent then
                            !recPositionHistory.contains(position)&&            //position not already selected
                            (recGridArray[position/size][position%size] == word.charAt(letterIndex))){  // letter exist, use it
                        recPositionHistory.add(position); //letter is added, onto next one
                     //   System.out.println("i am not useless!!!");
                        if (buildGameGridRec(recGridArray,word,wordIndex,letterIndex+1,recPositionHistory,iteration+"."+i)) {
                            return true;
                        }
                        else { //undo it and continue searching
                            recPositionHistory.remove(recPositionHistory.size()-1);
                        }
                    }
                    position = (position+1)%(size*size);
                }

                //if this fail, then create letter
                if(recPositionHistory.size() != 0){
                    int iVary = new Random().nextInt(3);
                    int jVary = new Random().nextInt(3);
                    int coreI = recPositionHistory.get(recPositionHistory.size()-1)/size;
                    int coreJ = recPositionHistory.get(recPositionHistory.size()-1)%size;
                    for (int i = 0; i < 3; i++){
                        for (int j = 0; j<3; j++){
                      //      System.out.println (coreI + "," + coreJ+ "\t+" + iVary + ",+" + jVary+ "\t\t" +(coreI+iVary-1)+ " " + (coreJ+jVary-1) );

                            if( isAdjacent(coreI*size+coreJ,(coreI+iVary-1),(coreJ+jVary-1)) && recGridArray[coreI+iVary-1][coreJ+jVary-1] == '0'   ){
                                //add letter here
                                recGridArray[coreI+iVary-1][coreJ+jVary-1] = word.charAt(letterIndex);
                                recPositionHistory.add((coreI+iVary-1)*size+(coreJ+jVary-1));
                        //        System.out.println ("added "+word.charAt(letterIndex) + " at " +(coreI+iVary-1)+ " " + (coreJ+jVary-1) );
                                //build next letter
                                if (buildGameGridRec(recGridArray,word,wordIndex,letterIndex+1,recPositionHistory,iteration+","+(i*3+j))) {
                                    return true;
                                }
                                else {
                                    recPositionHistory.remove(recPositionHistory.size()-1);
                                    recGridArray[coreI+iVary-1][coreJ+jVary-1] = '0';
                                }
                            }
                            jVary = (jVary+1) % 3;
                        }
                        iVary = (iVary+1) % 3;
                    }
                }
                else {          //need to get letter from anywhere random
                    int positionAny = new Random().nextInt(size*size);
                    for(int i = 0; i < size*size; i++){
                        if(recGridArray[positionAny/4][positionAny%4] == '0'){
                            recPositionHistory.add(positionAny);
                            recGridArray[positionAny/4][positionAny%4] = word.charAt(letterIndex);
                            if (buildGameGridRec(recGridArray,word,wordIndex,letterIndex+1,recPositionHistory,iteration+";"+i)) {
                                return true;
                            }
                            else {
                                recPositionHistory.remove(recPositionHistory.size()-1);
                                recGridArray[positionAny/4][positionAny%4] = '0';
                            }
                        }
                        positionAny = (positionAny+1)%(size*size);
                    }
                }
            }
            /*
            if (randNum == 1) { //create the letter first
                if(recPositionHistory.size() != 0){
                    int iVary = new Random().nextInt(3);
                    int jVary = new Random().nextInt(3);
                    int coreI = recPositionHistory.get(recPositionHistory.size()-1)/size;
                    int coreJ = recPositionHistory.get(recPositionHistory.size()-1)%size;
                    for (int i = 0; i < 3; i++){
                        for (int j = 0; j<3; j++){
                            //      System.out.println (coreI + "," + coreJ+ "\t+" + iVary + ",+" + jVary+ "\t\t" +(coreI+iVary-1)+ " " + (coreJ+jVary-1) );

                            if( isAdjacent(coreI*size+coreJ,(coreI+iVary-1),(coreJ+jVary-1)) && recGridArray[coreI+iVary-1][coreJ+jVary-1] == '0'   ){
                                //add letter here
                                recGridArray[coreI+iVary-1][coreJ+jVary-1] = word.charAt(letterIndex);
                                recPositionHistory.add((coreI+iVary-1)*size+(coreJ+jVary-1));
                                //        System.out.println ("added "+word.charAt(letterIndex) + " at " +(coreI+iVary-1)+ " " + (coreJ+jVary-1) );
                                //build next letter
                                if (buildGameGridRec(recGridArray,word,wordIndex,letterIndex+1,recPositionHistory,iteration+","+(i*3+j))) {
                                    return true;
                                }
                                else {
                                    recPositionHistory.remove(recPositionHistory.size()-1);
                                    recGridArray[coreI+iVary-1][coreJ+jVary-1] = '0';
                                }
                            }
                            jVary = (jVary+1) % 3;
                        }
                        iVary = (iVary+1) % 3;
                    }
                }
                else {          //need to get letter from anywhere random
                    int positionAny = new Random().nextInt(size*size);
                    for(int i = 0; i < size*size; i++){
                        if(recGridArray[positionAny/4][positionAny%4] == '0'){
                            recPositionHistory.add(positionAny);
                            recGridArray[positionAny/4][positionAny%4] = word.charAt(letterIndex);
                            if (buildGameGridRec(recGridArray,word,wordIndex,letterIndex+1,recPositionHistory,iteration+",."+i)) {
                                return true;
                            }
                            else {
                                recPositionHistory.remove(recPositionHistory.size()-1);
                                recGridArray[positionAny/4][positionAny%4] = '0';
                            }
                        }
                        positionAny = (positionAny+1)%(size*size);
                    }
                }
                //if fail, then other option, look for letter
                int position = new Random().nextInt(size*size);   // start at random point in grid to look for letter
                for(int i = 0; i < size*size; i++){
                    if(((recPositionHistory.size() == 0) ||             //can look anywhere since not last position
                            (recPositionHistory.size()!=0 && isAdjacent(recPositionHistory.get(recPositionHistory.size()-1),position/size, position%size)))&&    //has last position, must be adjacent then
                            !recPositionHistory.contains(position)&&            //position not already selected
                            (recGridArray[position/size][position%size] == word.charAt(letterIndex))){  // letter exist, use it
                        recPositionHistory.add(position); //letter is added, onto next one
                        if (buildGameGridRec(recGridArray,word,wordIndex,letterIndex+1,recPositionHistory,iteration+"."+i)) {
                            return true;
                        }
                        else { //undo it and continue searching
                            recPositionHistory.remove(recPositionHistory.size()-1);
                        }

                    }
                    position = (position+1)%(size*size);
                }
            }
  //          */
            return false;  //not possible, did everything already
        }
        else {      //build the next word
            return buildGameGridRec(recGridArray, possibleWords.get(wordIndex+1),wordIndex+1,0, null,""+wordIndex);
        }



    }
    public boolean isAdjacent(int core, int neighborI, int neighborJ){   //need testing
        int size = homeCircleGrid.length;

        int coreI = core/size;
        int coreJ = core%size;
        if (neighborI*size + neighborJ == core) {
     //       System.out.println("same point, should never happen");
            return false;
        }
    //    System.out.println(neighborI +"," + neighborJ + "  " + size);
        if (neighborI >= size || neighborI<0 || neighborJ>=size || neighborJ<0) {
      //      System.out.println("out bound, should never happen");
            return false;
        }

    //    System.out.println("Comparing: "+ ((Math.abs(neighborI-coreI) >= 1)&&(Math.abs(neighborJ-coreJ) >= 1)));
        return (Math.abs(neighborI-coreI) <= 1)&&(Math.abs(neighborJ-coreJ) <= 1);

    }


    public void reloadWorkspace(){}
    public void initStyle(){}
}
