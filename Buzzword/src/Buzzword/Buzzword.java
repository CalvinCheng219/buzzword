package Buzzword;

import GUI.Workspace;
import apptemplate.AppTemplate;
import components.AppComponentsBuilder;
import components.AppDataComponent;
import components.AppFileComponent;
import components.AppWorkspaceComponent;
import Data.GameData;
import Data.GameDataFile;

/**
 * Created by Calvin on 11/13/16.
 */
public class Buzzword extends AppTemplate {

        public static void main(String[] args) {
            launch(args);
        }

        public String getFileControllerClass() {
            return "BuzzWordController";
        }

        @Override
        public AppComponentsBuilder makeAppBuilderHook() {
            return new AppComponentsBuilder() {
                @Override
                public AppDataComponent buildDataComponent() throws Exception {
                    return new GameData(Buzzword.this);
                }

                @Override
                public AppFileComponent buildFileComponent() throws Exception {
                    return new GameDataFile();
                }

                @Override
                public AppWorkspaceComponent buildWorkspaceComponent() throws Exception {
                    return new Workspace(Buzzword.this);
                }
            };
        }
}


