package Data;

import apptemplate.AppTemplate;
import components.AppDataComponent;
import ui.AppMessageDialogSingleton;

import java.util.ArrayList;

/**
 * Created by Calvin on 11/13/16.
 */
public class GameData implements AppDataComponent {
    private String currentUsername;
    private String currentPassword;
    private ArrayList<String> wordPool;
    private ArrayList<String> modeList;
    private ArrayList<Integer> modeProgressList;
    private int[][] highScoreList;


    public AppTemplate appTemplate;

    public GameData(){

    }
    public GameData(AppTemplate at){
        at.setDataComponent(this);
        this.appTemplate = at;
        wordPool = new ArrayList<String>();
        modeList = new ArrayList<String>();
        modeProgressList = new ArrayList<Integer>();

        highScoreList = new int[4][8];
        GameDataFile gdf = (GameDataFile)(appTemplate.getFileComponent());
        try {
            gdf.loadGameModes(this);
        }catch (Exception e){
            AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
            dialog.show("ERROR", e.getMessage());
        }
    }
    public int[][] getHighScoreList(){
        return highScoreList;
    }
    public void setUsername(String username){
        currentUsername = username;
    }
    public void setPassword(String password){
        currentPassword = password;
    }
    public ArrayList<String> getWordPool(){
        return wordPool;
    }
    public ArrayList<String> getModeList(){
        return modeList;
    }
    public ArrayList<Integer> getModeProgressList(){
        return modeProgressList;
    }

    public String getUsername(){return currentUsername;}
    public String getPassword(){return currentPassword;}

    public void reset(){}
}
