package Data;

import components.AppDataComponent;
import components.AppFileComponent;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import ui.AppMessageDialogSingleton;

import java.io.*;
import java.net.URL;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Iterator;


import static settings.InitializationParameters.APP_WORKDIR_PATH;

/**
 * Created by Calvin on 11/13/16.
 */
public class GameDataFile implements AppFileComponent {
    private String key = "asdf";
    public GameDataFile() {

    }

    public String EncryptText(String s){
        String newWord = new String();
        int wordLen = s.length();
        int keyLen = key.length();
        for(int i = 0; i < s.length(); i++){
            newWord = newWord + (char)((s.charAt(i) + (int)(key.charAt(i%keyLen)))-50);
        }
        return newWord;
    }
    public String DecryptText(String s){
        String newWord = new String();
        int wordLen = s.length();
        int keyLen = key.length();
        for(int i = 0; i < s.length(); i++){
            newWord = newWord + (char)(s.charAt(i) - (int)(key.charAt(i%keyLen))+50);
        }
        return newWord;
    }



    public void loadWordPool(AppDataComponent data, String mode) throws  Exception{
        try {
            URL workDirURL = getClass().getClassLoader().getResource("words");
            if (workDirURL == null)
                throw new FileNotFoundException("Work folder not found under resources.");
            URL workingURL = new URL(workDirURL.toString() + "/" + mode+".txt");
            System.out.println(workingURL.toString());
            File workingFile = new File(workingURL.getFile());
            if (mode== null){
                throw new IOException("Please select a game mode first");
            }
            if (!workingFile.exists()){
                throw new IOException("Unable to find "+ mode+ ".txt");
            }
            GameData gd = (GameData)data;
            BufferedReader input =  new BufferedReader(new FileReader(workingFile));
            String line = null;
            gd.getWordPool().clear();
            while (( line = input.readLine()) != null) {
                gd.getWordPool().add(line.toLowerCase());
            }
            input.close();

        }catch (Exception e){
            throw e;
        }
    }


    public void loadGameModes(AppDataComponent data) throws Exception{
        JSONParser parser = new JSONParser();
        try {
            URL workDirURL = getClass().getClassLoader().getResource("words");
            if (workDirURL == null)
                throw new FileNotFoundException("Work folder not found under resources.");
            URL workingURL = new URL(workDirURL.toString() + "/" + "GameMode.json");
            System.out.println(workingURL.toString());
            File workingFile = new File(workingURL.getFile());
            if (!workingFile.exists()){
                throw new IOException("Unable to find GameMode.json");
            }

            Object obj = parser.parse(new FileReader(workingFile));
            JSONObject jsonObject = (JSONObject) obj;
            GameData gd = (GameData)data;
            JSONArray gameModes = (JSONArray) jsonObject.get("GameMode");
            Iterator<String> iterator = gameModes.iterator();
            ArrayList<String> modeList = gd.getModeList();
            modeList.clear();
            while (iterator.hasNext()) {
                modeList.add(iterator.next());
            }
            if(modeList.size() == 0){
                throw new IOException("Nothing in mode list");
            }

        } catch (ParseException e) {
            e.printStackTrace();
        }catch (Exception e){
            throw e;
        }
    }

    public void createProfile(AppDataComponent data, Path to) throws IOException{
        JSONObject obj = new JSONObject();
        GameData gd = ((GameData) data);
        //  System.out.println(gd.get)

        obj.put("username", gd.getUsername());
        obj.put("password", EncryptText(gd.getPassword()));


        JSONArray modeProgressList = new JSONArray();
        Iterator<Integer> modeProgressListIt = gd.getModeProgressList().iterator();
        while(modeProgressListIt.hasNext()){
            modeProgressList.add("" + modeProgressListIt.next());
        }
        obj.put("modeProgressList", modeProgressList);

        for(int i = 0 ; i < gd.getModeList().size(); i++) {
            JSONArray mode = new JSONArray();
            int[][] highScore = gd.getHighScoreList();
            for(int j = 0; j < 8; j++){
                mode.add("" + highScore[i][j]);
            }
            obj.put(gd.getModeList().get(i)+  "Score", mode);
        }




        try{
            FileWriter file = new FileWriter(to.toFile());
            file.write(obj.toJSONString());
            file.flush();
            file.close();
        }
        catch (IOException e){
            e.printStackTrace();
            throw new IOException("There was a problem with the file");
        }
    }

    public void loadProfile(AppDataComponent data, Path from) throws IOException{
        JSONParser parser = new JSONParser();
        try {

             Object obj = parser.parse(new FileReader(from.toFile()));
            JSONObject jsonObject = (JSONObject) obj;
            GameData gd = (GameData)data;
            if (gd.getUsername().contains(" ")){
                throw new IOException("Username cannot contain space");
            }
            if ( !gd.getPassword().equals( DecryptText( jsonObject.get("password").toString()))){
                throw new IOException("Wrong password");
            }

            JSONArray modeProgressList = (JSONArray) jsonObject.get("modeProgressList");
            Iterator<String> iterator = modeProgressList.iterator();
            gd.getModeProgressList().clear();
            while (iterator.hasNext()) {
                gd.getModeProgressList().add(Integer.parseInt(iterator.next()));
            }

            for (int i = 0; i < gd.getModeList().size(); i++) {
                JSONArray highScore = (JSONArray) jsonObject.get(gd.getModeList().get(i)+ "Score");
                Iterator<String> iteratorHigh = highScore.iterator();
                int[][] highScoreList = gd.getHighScoreList();
                int index = 0;
                while (iterator.hasNext()) {
                    highScoreList[i][index] = Integer.parseInt(iteratorHigh.next());
                }
            }




        } catch (ParseException e) {
            e.printStackTrace();
        }catch (Exception e){
            if (e.getMessage() == null){
                throw new IOException("Unable to load, corrupt file");
            }
            throw e;
        }
    }

    public void editProfile(AppDataComponent data, Path to) throws IOException{
        JSONObject obj = new JSONObject();
        GameData gd = ((GameData) data);
        //  System.out.println(gd.get)

        obj.put("username", gd.getUsername());
        obj.put("password", EncryptText( gd.getPassword()));

        JSONArray modeProgressList = new JSONArray();
        Iterator<Integer> modeProgressListIt = gd.getModeProgressList().iterator();
        while(modeProgressListIt.hasNext()){
            modeProgressList.add("" + modeProgressListIt.next());
        }
        obj.put("modeProgressList", modeProgressList);


        for(int i = 0 ; i < gd.getModeList().size(); i++) {
            JSONArray mode = new JSONArray();
            int[][] highScore = gd.getHighScoreList();
            for(int j = 0; j < 8; j++){
                mode.add("" + highScore[i][j]);
            }
            obj.put(gd.getModeList().get(i)+  "Score", mode);
        }

        try{
            FileWriter file = new FileWriter(to.toFile());
            file.write(obj.toJSONString());
            file.flush();
            file.close();
        }
        catch (IOException e){
            throw new IOException("There was a problem saving your progress");
        }
    }


    public void saveData(AppDataComponent data, Path to) {
        JSONObject obj = new JSONObject();
        obj.put("targetWord","bleh");

        GameData gd = ((GameData) data);
        //  System.out.println(gd.get)
        obj.put("username",gd.getUsername());
        obj.put("password", gd.getPassword());

        /*
        JSONArray goodGuess = new JSONArray();
        Iterator<Character> goodIt = gd.getGoodGuesses().iterator();
        while(goodIt.hasNext()){
            goodGuess.add("" + goodIt.next());
        }

        JSONArray badGuess = new JSONArray();
        Iterator<Character> badIt = gd.getBadGuesses().iterator();
        while(badIt.hasNext()){
            badGuess.add("" + badIt.next());
        }

        obj.put("goodGuesses",goodGuess);
        obj.put("badGuesses",badGuess);
        obj.put("hintable",gd.getHintable());

        */

        try{
            FileWriter file = new FileWriter(to.toFile());
            file.write(obj.toJSONString());
            file.flush();
            file.close();
        }
        catch (IOException e){
            AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
            dialog.show("Saving Error", "There was a problem with the file");
        }

    }


    public void loadData(AppDataComponent data, Path from) throws IOException {

    }


    public void exportData(AppDataComponent data, Path filePath) throws IOException {

    }


}
