package controller;

import Data.GameData;
import Data.GameDataFile;
import GUI.Workspace;
import apptemplate.AppTemplate;
import controller.FileController;
import javafx.animation.AnimationTimer;
import javafx.event.EventHandler;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyCodeCombination;
import javafx.scene.input.KeyCombination;
import javafx.scene.input.KeyEvent;
import javafx.stage.FileChooser;
import propertymanager.PropertyManager;
import ui.AppMessageDialogSingleton;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URL;
import java.nio.file.Paths;
import java.util.ArrayList;

import static settings.AppPropertyType.*;
import static settings.InitializationParameters.APP_WORKDIR_PATH;
import static sun.jvm.hotspot.runtime.PerfMemory.end;

/**
 * Created by Calvin on 11/13/16.
 */
public class BuzzWordController implements FileController {

    private File workingFile;
    public AppTemplate appTemplate;
    private AnimationTimer timer;
    public BuzzWordController(){

    }
    public BuzzWordController(AppTemplate at){
        this.appTemplate = at;
        at.getGUI().setFileController(this);
    }



    public void handleLogin() throws IOException {
        PropertyManager propertyManager = PropertyManager.getManager();
        AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
        Workspace ws =  (Workspace)appTemplate.getWorkspaceComponent();
        PropertyManager           props  = PropertyManager.getManager();

        try {
            URL workDirURL = AppTemplate.class.getClassLoader().getResource(APP_WORKDIR_PATH.getParameter());
            if (workDirURL == null)
                throw new FileNotFoundException("Work folder not found under resources.");


            URL workingURL = new URL(workDirURL.toString() + "/" + ws.usernameField.getText() +".json");
            workingFile = new File(workingURL.getFile());
            if (!workingFile.exists()){
                throw new IOException("This username does not exist");
            }
            GameDataFile gdf = (GameDataFile)appTemplate.getFileComponent();
            GameData gd = (GameData)appTemplate.getDataComponent();
            gd.setUsername(ws.usernameField.getText());
            gd.setPassword(ws.passwordField.getText());
            gdf.loadProfile(gd, Paths.get(workingFile.getAbsolutePath()));

        //    dialog.show(props.getPropertyValue(SAVE_COMPLETED_TITLE), props.getPropertyValue(SAVE_COMPLETED_MESSAGE));
            dialog.show("Load Complete", "Profile successfully loaded");

        }
        catch (IOException ioe) {
            //   dialog.show(propertyManager.getPropertyValue(SAVE_ERROR_TITLE), propertyManager.getPropertyValue(SAVE_ERROR_MESSAGE));
            throw ioe;
        }
    }

    public void handlePlay(){
        Workspace ws =  (Workspace)appTemplate.getWorkspaceComponent();

        timer = new AnimationTimer() {
            @Override
            public void handle(long now) {

                if(now - ws.before > 1000000000 && !ws.paused){
                    if ( ws.setTimer()){
                        System.out.println(ws.before + " " + now);
                        ws.playing = false;
                        ws.updateEndGameInfoLabels();
                        ws.handleEndGame(false);
                        stop();
                    }
                    ws.before = now;
                }

                if(ws.totalPoints >= ws.targetScore){
                    //yay, we win
                    ws.playing = false;
                    ws.handleEndGame(true);
                    stop();
                }
            }

            @Override
            public void stop() {
                super.stop();
              //  end();
            }
        };

        timer.start();
    }



    public void handleNewProfile() throws IOException {
        PropertyManager propertyManager = PropertyManager.getManager();
        AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
        Workspace ws =  (Workspace)appTemplate.getWorkspaceComponent();
        PropertyManager           props  = PropertyManager.getManager();

        try {
            URL workDirURL = AppTemplate.class.getClassLoader().getResource(APP_WORKDIR_PATH.getParameter());
            if (workDirURL == null)
                throw new FileNotFoundException("Work folder not found under resources.");


            URL workingURL = new URL(workDirURL.toString() + "/" + ws.setUsernameField.getText() + ".json");
            workingFile = new File(workingURL.getFile());
            if (workingFile.exists()){
                throw new IOException("Username Already Exist");
            }
            GameDataFile gdf = (GameDataFile)appTemplate.getFileComponent();
            GameData gd = (GameData)appTemplate.getDataComponent();
            gd.setUsername(ws.setUsernameField.getText());
            gd.setPassword(ws.setPasswordField.getText());
            gd.getModeProgressList().clear();
            int[][] highScoreList = new int[4][8];
            for(int i = 0; i < gd.getModeList().size(); i++){
                gd.getModeProgressList().add(1);
                for( int j = 0; j < 8; j++){
                    highScoreList[i][j] = 0;
                }
            }
            gdf.createProfile(gd, Paths.get(workingFile.getAbsolutePath()));




           // dialog.show(props.getPropertyValue(SAVE_COMPLETED_TITLE), props.getPropertyValue(SAVE_COMPLETED_MESSAGE));
        //    dialog.show("Save Complete", "Profile is up to date");

        }
         catch (IOException ioe) {
             ioe.printStackTrace();
         //   dialog.show(propertyManager.getPropertyValue(SAVE_ERROR_TITLE), propertyManager.getPropertyValue(SAVE_ERROR_MESSAGE));
            throw ioe;
        }
    }



    @Override
    public void handleNewRequest() {

    }

    public void handleEditRequest() throws IOException {
        PropertyManager propertyManager = PropertyManager.getManager();
        AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
        Workspace ws =  (Workspace)appTemplate.getWorkspaceComponent();
        PropertyManager           props  = PropertyManager.getManager();

        try {
            if (!workingFile.exists()){
                throw new IOException("Save file is missing");
            }
            GameDataFile gdf = (GameDataFile)appTemplate.getFileComponent();
            GameData gd = (GameData)appTemplate.getDataComponent();
            gdf.editProfile(gd, Paths.get(workingFile.getAbsolutePath()));

            // dialog.show(props.getPropertyValue(SAVE_COMPLETED_TITLE), props.getPropertyValue(SAVE_COMPLETED_MESSAGE));
  //          dialog.show("Save Complete", "Profile created");

        }
        catch (IOException ioe) {
            //   dialog.show(propertyManager.getPropertyValue(SAVE_ERROR_TITLE), propertyManager.getPropertyValue(SAVE_ERROR_MESSAGE));
            throw ioe;
        }
    }


    @Override
    public void handleSaveRequest() throws IOException {
        PropertyManager propertyManager = PropertyManager.getManager();
        try {
            //          if (currentWorkFile!= null)
            //             saveWork(currentWorkFile);
            //          else {
            FileChooser fileChooser = new FileChooser();


            //    boolean test = new File(AppTemplate.class.getClassLoader().getResource(APP_WORKDIR_PATH.getParameter()).mkdir();
            // System.out.println(AppTemplate.class.getClassLoader().getResource(APP_WORKDIR_PATH.getParameter()));
            URL workDirURL = AppTemplate.class.getClassLoader().getResource(APP_WORKDIR_PATH.getParameter());
            if (workDirURL == null)
                throw new FileNotFoundException("Work folder not found under resources.");

            File initialDir = new File(workDirURL.getFile());

            // File initialDir = new File("C:\\Users\\Andy\\Documents\\TheHangmanGame\\TheHangmanGame\\Hangman\\saved");
            fileChooser.setInitialDirectory(initialDir);
            fileChooser.setTitle(propertyManager.getPropertyValue(SAVE_WORK_TITLE));
            fileChooser.getExtensionFilters().add(new FileChooser.ExtensionFilter(".Json","json"));
           // fileChooser.getExtensionFilters().addAll(new FileChooser.ExtensionFilter(propertyManager.getPropertyValue(WORK_FILE_EXT_DESC),
           //         propertyManager.getPropertyValue(WORK_FILE_EXT)));
            File selectedFile = fileChooser.showSaveDialog(appTemplate.getGUI().getWindow());
            if (selectedFile != null) {
                String ext = "";
                // System.out.println("this one " + selectedFile.toString());
                int i = selectedFile.toString().lastIndexOf('.');
                if (i > 0) {
                    ext = selectedFile.toString().substring(i + 1);
                }

       //         if (ext.equalsIgnoreCase("json")) {
                    saveWork(selectedFile);
       /*             //         savable = false;
                    //        appTemplate.getGUI().updateWorkspaceToolbar(savable, loadable);

                } else {
                    AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
                    dialog.show("Save Error", "Invalid file format");
                }
                */

            }

        } catch (IOException ioe) {
            AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
            dialog.show(propertyManager.getPropertyValue(SAVE_ERROR_TITLE), propertyManager.getPropertyValue(SAVE_ERROR_MESSAGE));
               ioe.printStackTrace();
        }
    }

    private void saveWork(File selectedFile) throws IOException {
        appTemplate.getFileComponent()
                .saveData(appTemplate.getDataComponent(), Paths.get(selectedFile.getAbsolutePath()));

   //     currentWorkFile = selectedFile;
   //     saved = true;

        AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
        PropertyManager           props  = PropertyManager.getManager();
        dialog.show(props.getPropertyValue(SAVE_COMPLETED_TITLE), props.getPropertyValue(SAVE_COMPLETED_MESSAGE));
    }

    @Override
    public void handleLoadRequest() throws IOException {

    }

    @Override
    public void handleExitRequest() {

    }

    @Override
    public void handleInfoRequest() {

    }
}
